PGDMP     $                     w         
   db_escolas #   10.6 (Ubuntu 10.6-0ubuntu0.18.04.1) #   10.6 (Ubuntu 10.6-0ubuntu0.18.04.1) #    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16454 
   db_escolas    DATABASE     |   CREATE DATABASE db_escolas WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pt_BR.UTF-8' LC_CTYPE = 'pt_BR.UTF-8';
    DROP DATABASE db_escolas;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    13043    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16573    tblgestores    TABLE       CREATE TABLE public.tblgestores (
    codigo_imovel integer NOT NULL,
    tipo_eleito character varying(45) DEFAULT NULL::character varying,
    cargo character varying(45) DEFAULT NULL::character varying,
    situacao_cargo character varying(45) DEFAULT NULL::character varying,
    matricula character varying(45) DEFAULT NULL::character varying,
    nome character varying(45) DEFAULT NULL::character varying,
    cpf character varying(45) DEFAULT NULL::character varying,
    telefone character varying(45) DEFAULT NULL::character varying,
    celular character varying(45) DEFAULT NULL::character varying,
    celular2 character varying(45) DEFAULT NULL::character varying,
    email character varying(45) DEFAULT NULL::character varying,
    id integer NOT NULL
);
    DROP TABLE public.tblgestores;
       public         postgres    false    3            �            1259    16603    tblgestores_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tblgestores_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.tblgestores_id_seq;
       public       postgres    false    3    197            �           0    0    tblgestores_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.tblgestores_id_seq OWNED BY public.tblgestores.id;
            public       postgres    false    198            �            1259    16533 
   tblimoveis    TABLE     	  CREATE TABLE public.tblimoveis (
    codigo integer NOT NULL,
    codigo_mae character varying(45) DEFAULT NULL::character varying,
    regional character varying(45) DEFAULT NULL::character varying,
    cidade character varying(45) DEFAULT NULL::character varying,
    inep character varying(45) DEFAULT NULL::character varying,
    anexo character varying(45) DEFAULT NULL::character varying,
    descricao character varying(300) DEFAULT NULL::character varying,
    nome character varying(45) DEFAULT NULL::character varying,
    modalidade character varying(45) DEFAULT NULL::character varying,
    situacao character varying(45) DEFAULT NULL::character varying,
    ocupacao character varying(45) DEFAULT NULL::character varying,
    educacao_infantil_presencial character varying(45) DEFAULT NULL::character varying,
    anos_finais_presencial character varying(45) DEFAULT NULL::character varying,
    ensino_medio_presencial character varying(45) DEFAULT NULL::character varying,
    educacao_profissional_presencial character varying(45) DEFAULT NULL::character varying,
    eja_presencial character varying(45) DEFAULT NULL::character varying,
    eja_semipresencial character varying(45) DEFAULT NULL::character varying,
    magisterio_ead character varying(45) DEFAULT NULL::character varying,
    eja_ead character varying(45) DEFAULT NULL::character varying,
    educacao_presencial_ead character varying(45) DEFAULT NULL::character varying,
    total_escolarizacao character varying(45) DEFAULT NULL::character varying,
    cep character varying(45) DEFAULT NULL::character varying,
    logradouro character varying(45) DEFAULT NULL::character varying,
    numero character varying(45) DEFAULT NULL::character varying,
    complemento character varying(45) DEFAULT NULL::character varying,
    bairro character varying(45) DEFAULT NULL::character varying,
    data_cadastro character varying(45) DEFAULT NULL::character varying,
    ultima_atualizacao character varying(45) DEFAULT NULL::character varying,
    latitude character varying(45) DEFAULT NULL::character varying,
    longitude character varying(45) DEFAULT NULL::character varying,
    anos_iniciais_presencial character varying(45) DEFAULT NULL::character varying,
    educacao_profissional_ead character varying(45) DEFAULT NULL::character varying
);
    DROP TABLE public.tblimoveis;
       public         postgres    false    3            �            1259    16611    tblrepasses    TABLE     <  CREATE TABLE public.tblrepasses (
    codigo_imovel integer NOT NULL,
    tipo_repasse character varying(45) DEFAULT NULL::character varying,
    codigo_caixa_escolar character varying(45) DEFAULT NULL::character varying,
    ano_parcela character varying(45) DEFAULT NULL::character varying,
    nivel_ensino character varying(45) DEFAULT NULL::character varying,
    etapa_ensino character varying(45) DEFAULT NULL::character varying,
    descricao_repasse character varying(300) DEFAULT NULL::character varying,
    numero_parcela character varying(45) DEFAULT NULL::character varying,
    valor character varying(45) DEFAULT NULL::character varying,
    numero_processo character varying(45) DEFAULT NULL::character varying,
    ano_processo character varying(45) DEFAULT NULL::character varying,
    id integer NOT NULL
);
    DROP TABLE public.tblrepasses;
       public         postgres    false    3            �            1259    16631    tblrepasses_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tblrepasses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.tblrepasses_id_seq;
       public       postgres    false    199    3            �           0    0    tblrepasses_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.tblrepasses_id_seq OWNED BY public.tblrepasses.id;
            public       postgres    false    200            �            1259    16652    tblterceirizados    TABLE     �  CREATE TABLE public.tblterceirizados (
    codigo_imovel integer,
    tipo character varying(45) DEFAULT NULL::character varying,
    posto_trabalho character varying(45) DEFAULT NULL::character varying,
    quantidade integer,
    valor_unitario character varying(45) DEFAULT NULL::character varying,
    data_ultima_atualizacao character varying(45) DEFAULT NULL::character varying,
    id integer NOT NULL
);
 $   DROP TABLE public.tblterceirizados;
       public         postgres    false    3            �            1259    16659    tblterceirizados_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tblterceirizados_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.tblterceirizados_id_seq;
       public       postgres    false    201    3            �           0    0    tblterceirizados_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.tblterceirizados_id_seq OWNED BY public.tblterceirizados.id;
            public       postgres    false    202                       2604    16605    tblgestores id    DEFAULT     p   ALTER TABLE ONLY public.tblgestores ALTER COLUMN id SET DEFAULT nextval('public.tblgestores_id_seq'::regclass);
 =   ALTER TABLE public.tblgestores ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    197            *           2604    16633    tblrepasses id    DEFAULT     p   ALTER TABLE ONLY public.tblrepasses ALTER COLUMN id SET DEFAULT nextval('public.tblrepasses_id_seq'::regclass);
 =   ALTER TABLE public.tblrepasses ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    199            /           2604    16661    tblterceirizados id    DEFAULT     z   ALTER TABLE ONLY public.tblterceirizados ALTER COLUMN id SET DEFAULT nextval('public.tblterceirizados_id_seq'::regclass);
 B   ALTER TABLE public.tblterceirizados ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    201            �          0    16573    tblgestores 
   TABLE DATA               �   COPY public.tblgestores (codigo_imovel, tipo_eleito, cargo, situacao_cargo, matricula, nome, cpf, telefone, celular, celular2, email, id) FROM stdin;
    public       postgres    false    197   �6       �          0    16533 
   tblimoveis 
   TABLE DATA               �  COPY public.tblimoveis (codigo, codigo_mae, regional, cidade, inep, anexo, descricao, nome, modalidade, situacao, ocupacao, educacao_infantil_presencial, anos_finais_presencial, ensino_medio_presencial, educacao_profissional_presencial, eja_presencial, eja_semipresencial, magisterio_ead, eja_ead, educacao_presencial_ead, total_escolarizacao, cep, logradouro, numero, complemento, bairro, data_cadastro, ultima_atualizacao, latitude, longitude, anos_iniciais_presencial, educacao_profissional_ead) FROM stdin;
    public       postgres    false    196   �8       �          0    16611    tblrepasses 
   TABLE DATA               �   COPY public.tblrepasses (codigo_imovel, tipo_repasse, codigo_caixa_escolar, ano_parcela, nivel_ensino, etapa_ensino, descricao_repasse, numero_parcela, valor, numero_processo, ano_processo, id) FROM stdin;
    public       postgres    false    199   R:       �          0    16652    tblterceirizados 
   TABLE DATA               �   COPY public.tblterceirizados (codigo_imovel, tipo, posto_trabalho, quantidade, valor_unitario, data_ultima_atualizacao, id) FROM stdin;
    public       postgres    false    201   �D       �           0    0    tblgestores_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.tblgestores_id_seq', 18, true);
            public       postgres    false    198            �           0    0    tblrepasses_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tblrepasses_id_seq', 330, true);
            public       postgres    false    200            �           0    0    tblterceirizados_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.tblterceirizados_id_seq', 18, true);
            public       postgres    false    202            3           2606    16610    tblgestores id 
   CONSTRAINT     L   ALTER TABLE ONLY public.tblgestores
    ADD CONSTRAINT id PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.tblgestores DROP CONSTRAINT id;
       public         postgres    false    197            1           2606    16571    tblimoveis tblimoveis_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tblimoveis
    ADD CONSTRAINT tblimoveis_pkey PRIMARY KEY (codigo);
 D   ALTER TABLE ONLY public.tblimoveis DROP CONSTRAINT tblimoveis_pkey;
       public         postgres    false    196            5           2606    16641    tblrepasses tblrepasses_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.tblrepasses
    ADD CONSTRAINT tblrepasses_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.tblrepasses DROP CONSTRAINT tblrepasses_pkey;
       public         postgres    false    199            7           2606    16666 &   tblterceirizados tblterceirizados_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.tblterceirizados
    ADD CONSTRAINT tblterceirizados_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.tblterceirizados DROP CONSTRAINT tblterceirizados_pkey;
       public         postgres    false    201            8           2606    16647 *   tblgestores tblgestores_codigo_imovel_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tblgestores
    ADD CONSTRAINT tblgestores_codigo_imovel_fkey FOREIGN KEY (codigo_imovel) REFERENCES public.tblimoveis(codigo);
 T   ALTER TABLE ONLY public.tblgestores DROP CONSTRAINT tblgestores_codigo_imovel_fkey;
       public       postgres    false    196    197    2865            9           2606    16642 *   tblrepasses tblrepasses_codigo_imovel_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tblrepasses
    ADD CONSTRAINT tblrepasses_codigo_imovel_fkey FOREIGN KEY (codigo_imovel) REFERENCES public.tblimoveis(codigo);
 T   ALTER TABLE ONLY public.tblrepasses DROP CONSTRAINT tblrepasses_codigo_imovel_fkey;
       public       postgres    false    2865    196    199            :           2606    16667 4   tblterceirizados tblterceirizados_codigo_imovel_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tblterceirizados
    ADD CONSTRAINT tblterceirizados_codigo_imovel_fkey FOREIGN KEY (codigo_imovel) REFERENCES public.tblimoveis(codigo);
 ^   ALTER TABLE ONLY public.tblterceirizados DROP CONSTRAINT tblterceirizados_codigo_imovel_fkey;
       public       postgres    false    2865    196    201            �   �  x����n� ���`�.��w��M*��N��R��-�o���N������ �W�)��2�B��e'z	c�r|ƅeVp���@h�Mg����� >x��{!Mk�p�r�?P���1�hܙ�۷�"f !�5�B��90�T��q��A
͝W��}���tN�Vg��S�[���3ƴsA�����ڗ���3ՔB�Ca#�O��0 ��3�պ�h��ؿU<�'���r� ��K(���@!=�4�8m��Q��y����M��Τ���B��#��U�=9/�پ�s�B�4�%�v���0��
n�ຊ��.Fv���iO�R��8���:��^+���W]ʛk��}�o�j�ʚ�K��3��{%m����?\S�S ��L`�����I�o~�i~1�Լ      �   �  x���ے� ��ۧ��i����U�uj/���c!N��d+��C�e�O�1`L4�v��f�#��HS BGj��a`�c��|?@�k������� ���� �c4�O�P�`�� ��9"�Z]W���*�� �>ǖp�~`���������8
��_1�KLK�."W�J͵�JK)��$e5W��YYiH�D5����Q���
���{��|��H>����M?�Tv���?TC��K�x�h�����|�5���˽2G�#���K!�YmEt
]�F���Blc:�y���@[z�$=�.��>)B��O�(��E��˃���w�J��d@�= 3�!��k��}V���DǸ�*9l�W�C��?�K��o�➜�*}Ue5uc,��-��o�3�����-��7ȟ�'�R�J(ܣ�=��"˲1�n      �   t
  x�ݝYr�F���S��ڗGXD��A*sB�'�*�lI~$	$`R�%$dm�_��,�B(^���P����B	鋗z�mʺ�q�m��zw���B��W�Z����4�~�ڶ.���[z~���{ZRۨ�-�B>��Ft���DՖO��Ϫ�ۧ���mn�*�rS�?��u��ḩv�m�Dj��Q[x}��`;�`rO
ib����1Sh�1���u���4�1s~̤���}��ާM�n�j����|��Q��,Ր��/bQ��;�YN���-\6�Z�얷)u���7���6�?iMRw���q�6����uO���tlu�ϋ�Z�yQ�����oۧn��]��J���K?�z��v��4���^��(�.zj��,u����玽��9ֻ�=������z�{��V�ݴ}�P�j���i�g��������ܿ�������h�&}=s�S��Ӈ4{������|��%'_K?�|Z�?��R���{IKݴ��щBZ)}?�HM���M9����������ϣ&��Ois/��E��.[أ]"�win?�%	��^�o�d��7�$�4=>�>>�>M�BL�b�L��I����`��PT�ê��ՠiĲ_�;��v!�)|���?��Y�[�R�/�eͶi�e�ϴ��my�m�_��L~��>uq��I��
��:W��vQ��d�gU��U���Pl� h������-�C8���>��Ě�b�����jicEa�酲B�rW�To[}��(�*r�)�޶;�Fq�>ni�6_l�$7��n=���~������~�q��M�{�15�Q��vؾ��*�e�����&g������.{Fr���D�cO�B`�W:�����L8M�F|�W���\c��	�N��G_�~�'���)��o���dE;! 5�dv���|������&w����\@Ġ��`�( ��M�Qh��4T��A,[W@,`��A���4Y�"�  -�{i�H���& -�����.igI4��`>F-
���(��ŨÈ`Ǩ#�1���:��b6F�nb:F���:6�:�6�:�	�<�]�*��A�;�tAf0�t�w0������=Y ���{{�y�'w���p�y�q�ef�1wV��� �����L����L��l]��a��,�L��,�L��,�L��|71]@�x2�e��2e��2"L�
�xɫLq}�W@F� 3( #�vB@F���% #��O�1޾�L1~k
R
��%�d�w��?̞5O	2-��"� k.�e�d�e����|����+v%)Yv�.%%<�ʠ%%<�� &%<�ʠ&e���夔d�e����+����x������(����<)�����(!*����O*U��O�}�6�D���p�Lj%��)��ᛷ#��'�Q
�p�&���|��<�_/j�`�f��6�?�]'����l [WCʽC*�h�y��W@d����Py�o�i�ޭ~����y����	p���d9�s��ZWy��'?�P�rM�c��h�����34�#$a�umni��ґ�i�\�BzF�0dn�e� �q<�ɡS�tl����G"�5�H������
��>eF��wD0��f����\��&���`�:,��j��fR�inF`��=)�2?L:�Bp5n)�L`��<J��h_���`�/Us7|a�#g�K.���9���8�qS���\�����E�$�u^�mM�������z�"g`�J��]��s��r�'�z=�к�^?©qx�� Aŀ�x4WG@���&���_�m  И�~�#@�1��^G>���g>���>Tv�>"�Ǳ�(I�3�}D���qPO��ɏa�w�#R9�ُ���FZ%����#�s�!%��3g��L�&8������S	}�S	Z�-8������K��#,ZQp*Ags3.�d�r��A	���rD��_<(a�q����|PB��q���,฀P�{�JBX�	N%!3��R�.!��kA�*H�*�[]!V��?`���UPTp V����XE��X-���2����5�W��ץ< ��D_P������^Ӊ [F����ވ{���
V�����)����������H5{��4t�g��4t�g��4�o+N3"t����<s~� ]�!8-]�!8-��c���.���;����;��Bt�
N{��R�iG�:̂�]P����.�	NG����3� 8�Y�Op���~g������������'>p� 8=]�.8�A��:�������.���pY~��vv��<˽u�����l�����	�@��e�ze��,����\)���T��}&I�kU�3}^M�U`�x�ʗ�.g<B��T�b�x�T�
�q7��1Y�oP]���婃,��D��R��p��������\oA��yAE���7�hZ�e#P�!S����c�5Z l����C&w���i��q��Z�pg�(ι�$�7Iˬ�(���T�D1�t��,b[Ks;ہ\4����A�k�`N�%b��`D�N!6pL�B@�LZ��粯o'�\c�ڀG�)�SZ��_��U���]!D�H���=><<���r�      �   �   x���An� ��5����`f`�j�E�q�]T��9��t�DV�W�>��/3�c�ߧ�(,�u�i�9�� �{�Z�� ���C3(���	��b��)�SZ+E�XR��2@0��7WEo�gzMǘ_Rc�X���u��Rꁜ��>�S�Ƙ������ޖ9�T�� ^i��c�qt�*�{�pa����{�d�؉6��oJ{'_1�vB��l�xq��j���2���
�ƴ�¡r���ݵ?=޳��o%���c	a     