<?php
//ROTAS DA API
Route::group(array('prefix' => 'api'), function(){

    Route::resource('dados', 'EscolaController');

});


//ROTA INICIAL DO SISTEMA
Route::get('/', function () {
    return view('index');
});














//ROTAS COM A FINALIDADE DE FACILITAR A INSERÇÃO DE DADOS AO BANCO
Route::get('/upload', function () {
    return view('upload');
});

Route::post('/upload-imovel', 'CarregaArquivo@carregarImovel');
Route::post('/upload-gestor', 'CarregaArquivo@carregarGestor');
Route::post('/upload-terceirizado', 'CarregaArquivo@carregarTerceirizado');
Route::post('/upload-repasse', 'CarregaArquivo@carregarRepasse');
