<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Imovel;
use App\Models\Gestor;
use App\Models\Terceirizado;
use App\Models\Repasse;

class CarregaArquivo extends Controller
{

    public function carregarGestor(Request $request){

        $arquivo = $request->arquivo;

        $row = 1;
        
        
        if (($handle = fopen($arquivo, "r")) !== FALSE) {
            
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    
                if($row != 1){

                    $gestor = new Gestor();

                    $gestor->codigo_imovel = $data[0];
                    $gestor->tipo_eleito = $data[1];
                    $gestor->cargo = $data[2];
                    $gestor->situacao_cargo = $data[3];
                    $gestor->matricula = $data[4];
                    $gestor->nome = $data[5];
                    $gestor->cpf = $data[6];
                    $gestor->telefone = $data[7];
                    $gestor->celular = $data[8];
                    $gestor->celular2 = $data[9];
                    $gestor->email = $data[10];
                    $gestor->save();
                        
                }

                $row++;
                
            }
            
            fclose($handle);
        }
        
    }

    public function carregarImovel(Request $request){

        $arquivo = $request->arquivo;

        $row = 1;
        
        
        if (($handle = fopen($arquivo, "r")) !== FALSE) {
            
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    
                if($row != 1){

                    $imovel = new Imovel();

                    $imovel->codigo = $data[0];
                    $imovel->codigo_mae = $data[1];
                    $imovel->regional = $data[2];
                    $imovel->cidade = $data[3];
                    $imovel->inep = $data[4];
                    $imovel->anexo = $data[5];
                    $imovel->descricao = $data[6];
                    $imovel->nome = $data[7];
                    $imovel->modalidade = $data[8];
                    $imovel->situacao = $data[9];
                    $imovel->ocupacao = $data[10];
                    $imovel->educacao_infantil_presencial = $data[11];
                    $imovel->anos_finais_presencial = $data[12];
                    $imovel->ensino_medio_presencial = $data[13];
                    $imovel->educacao_profissional_presencial = $data[14];
                    $imovel->eja_presencial = $data[15];
                    $imovel->eja_semipresencial = $data[16];
                    $imovel->magisterio_ead = $data[17];
                    $imovel->eja_ead = $data[18];
                    $imovel->educacao_presencial_ead = $data[19];
                    $imovel->total_escolarizacao = $data[20];
                    $imovel->cep = $data[21];
                    $imovel->logradouro = $data[22];
                    $imovel->numero = $data[23];
                    $imovel->complemento = $data[24];
                    $imovel->bairro = $data[25];
                    $imovel->data_cadastro = $data[26];
                    $imovel->ultima_atualizacao = $data[27];
                    $imovel->latitude = $data[28];
                    $imovel->longitude = $data[29];
                    $imovel->anos_iniciais_presencial = $data[30];
                    $imovel->educacao_profissional_ead = $data[31];
                                  
                    $imovel->save();
  
                    }

                $row++;
                
            }
            
            fclose($handle);
        }
        
    }

    public function carregarTerceirizado(Request $request){

        $arquivo = $request->arquivo;

        $row = 1;
        
        
        if (($handle = fopen($arquivo, "r")) !== FALSE) {
            
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    
                if($row != 1){

                    $terceirizado = new Terceirizado();

                    $terceirizado->codigo_imovel = $data[0];
                    $terceirizado->tipo = $data[1];
                    $terceirizado->posto_trabalho = $data[2];
                    $terceirizado->quantidade = $data[3];
                    $terceirizado->valor_unitario = $data[4];
                    $terceirizado->data_ultima_atualizacao = $data[5];
                    

                    
                    $terceirizado->save();
                        
                    }

                $row++;
                
            }
            
            fclose($handle);
        }
        
    }

    public function carregarRepasse(Request $request){

        $arquivo = $request->arquivo;

        $row = 1;
        
        
        if (($handle = fopen($arquivo, "r")) !== FALSE) {
            
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    
                if($row != 1){

                    $repasses = new Repasse();

                    $repasses->codigo_imovel = $data[0];
                    $repasses->tipo_repasse = $data[1];
                    $repasses->codigo_caixa_escolar = $data[2];
                    $repasses->ano_parcela = $data[3];
                    $repasses->nivel_ensino = $data[4];
                    $repasses->etapa_ensino = $data[5];
                    $repasses->descricao_repasse = $data[6];
                    $repasses->numero_parcela = $data[7];
                    $repasses->valor = $data[8];
                    $repasses->numero_processo = $data[9];
                    $repasses->ano_processo = $data[10];                  

                    
                    $repasses->save();
                        
                    }

                $row++;
                
            }
            
            fclose($handle);
        }
        
    }
}
