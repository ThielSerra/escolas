<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imovel extends Model
{
    protected $table = "tblimoveis";
    protected $primaryKey = "codigo";
    public $timestamps = false;


//RELACIONAMENTOS
    public function gestor(){
        return $this->hasMany(Gestor::class, 'codigo_imovel', 'codigo');
    }

    public function terceirizados(){
        return $this->hasMany(Terceirizado::class, 'codigo_imovel', 'codigo');
    }

    public function repasses(){
        return $this->hasMany(Repasse::class, 'codigo_imovel', 'codigo');
    }

}
