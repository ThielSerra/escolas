<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Imovel;

class Gestor extends Model
{
    protected $table = "tblgestores";
    protected $primaryKey = "id";
    public $timestamps = false;
    

//RELACIONAMENTO
    // public function imovel(){
    //     return $this->hasOne(Imovel::class, 'codigo_imovel', 'codigo');
    // }
    
}
