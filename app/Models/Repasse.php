<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Repasse extends Model
{
    protected $table = "tblrepasses";
    protected $primaryKey = "id";
    public $timestamps = false;


//RELACIONAMENTO
    // public function imovel(){
    //     return $this->hasOne('App\models\Imovel', 'codigo', 'codigo_imovel');
    // }
}
