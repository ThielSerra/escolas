<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Terceirizado extends Model
{
    protected $table = "tblterceirizados";
    protected $primaryKey = "id";
    public $timestamps = false;

    
//RELACIONAMENTO
    // public function imovel(){
    //     return $this->hasOne('App\models\Imovel', 'codigo', 'codigo_imovel');
    // }
}
