<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

        <title>Escolas - dados</title>

    </head>
    <body>
        <div  id="app" class="container">
        <!--EXIBE TELA COM LISTA DAS ESCOLAS CADASTRADAS-->
            <div v-if="!exibeDetalhamento">               
                <div class="row">
                    <div class="col-8">
                        <h2>Detalhamento escolar</h2>
                        
                        <br>

                        <ul>  
                            <div v-for="escola in escolas" >
                                <button type="button" v-on:click.prevent="visualizar(escola)" class="btn btn-info btn-lg btn-block"> 
                                    @{{ escola.descricao}} @{{escola.nome}}
                                </button> <br>
                            </div>
                        </ul>

                    </div>
                </div>
            </div>

        <!--EXIBE DETALHAMENTO DAS INFORMAÇÕES DAS ESCOLAS APÓS O CLICK-->
            <div v-else>

                <h4>Detalhamento da escola</h4>  

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="" v-on:click="getUrlApi">Início</a></li>
                        <li class="breadcrumb-item active" aria-current="page"></li>
                    </ol>
                </nav>

                <br><br>

                <div class="container">
                    <!--DADOS GERAIS DA ESCOLA-->
                    <div class="row">
                        <div class="col-8">
                            <!-- <ul v-for="item in escola"> -->
                                <div class="row">
                                    <div class="col-12">
                                        <h2> @{{escola.descricao}} @{{escola.nome}} </h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        Código INEP: @{{escola.inep}}
                                    </div>
                                    <div class="col-4">
                                        CNPJ: @{{escola.cnpj}}
                                    </div>
                                </div>
                            <!-- </ul> -->
                        </div>
                        <!-- <div class="col-4">
                            <label>Status escola</label> <br>
                            <button type="button" class="btn btn-success">ATIVO</button>
                        </div> -->
                        <div class="card text-center">
                                <div class="card-header">
                                    Status escola
                                </div>
                                <div class="card-footer" style="background: #29bb29;">
                                    ATIVO
                                </div>
                            </div>
                    </div>
                    
                    <br>
                    <!--ENDEREÇO-->
                    <div class="row">
                        <div class="col-9" style="background-color: #f7f7f7">
                            <!-- <ul v-for="item in escola" style="background-color: #f7f7f7"> -->
                                <div class="row">
                                    <div class="col-6">
                                        <label>Endereço:</label> <br>
                                        @{{ escola.logradouro }}
                                    
                                    </div>
                                    <div class="col-3">
                                        <label>Bairro:</label> <br>
                                        @{{ escola.bairro }}
                                    </div>
                                    <div class="col-3">
                                        <label>CEP:</label> <br>
                                        @{{ escola.cep }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <label>Localidade:</label> <br>
                                        @{{ escola.cidade }}
                                    
                                    </div>
                                    <div class="col-3">
                                        <label>Telefone:</label> <br>
                                        @{{ escola.telefone }}
                                    </div>
                                    <div class="col-3">
                                        <label>Regional:</label> <br>
                                        @{{ escola.regional }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <label>Dados do imóvel:</label> <br>
                                        @{{ escola.ocupacao }}
                                    </div>
                                    <div class="col-3">
                                        <label>Tipo escola:</label> <br>
                                        <!-- @{{ escola.bairro }} -->
                                    </div>
                                    <div class="col-3">
                                        <label>Tipo de ensino:</label> <br>
                                        @{{ escola.modalidade }}
                                    </div>
                                </div>
                            <!-- </ul> -->
                        </div>
                        <div class="col-3">
                        <!-- FALTA ESSE TRECHO -->
                            <div class="card text-center">
                                <div class="card-header">
                                    Vagas na escola
                                </div>
                                <div class="card-body">
                                    900
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <br>
                    <!--GESTOR-->
                    <h5>Dados do Gestor</h3> 
                    
                    <br>

                    <div class="row">
                        <div class="col-9" style="background-color: #f7f7f7">
                            <div class="row">
                                <div class="col-2">
                                    <!-- <img src="{{asset('/img/gestor-img.png')}}" alt=""> -->
                                </div>
                                <div class="col-10">
                                    <!-- <ul v-for="item in escola" style="background-color: #f7f7f7"> -->
                                        <div class="row">
                                            <div class="col-9">
                                                <label> Nome: </label> <br>
                                                @{{escola.gestor[0].nome}}
                                            </div>
                                            <div class="col-3">
                                                <label> Tipo: </label> <br>
                                                @{{escola.gestor[0].tipo_eleito}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label> Email: </label> <br>
                                                @{{escola.gestor[0].email}}
                                            </div>
                                            <div class="col-3">
                                                <label> Telefone: </label> <br>
                                                @{{escola.gestor[0].celular}}
                                            </div>
                                            <div class="col-3">
                                                <label> Formaçao: </label> <br>
                                            </div>
                                        </div>
                                    <!-- </ul> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                        <!-- FALTA ESSE TRECHO -->
                            <div class="card text-center">
                                <div class="card-header">
                                    Tipo de escola
                                </div>
                                <div class="card-body">
                                    Mãe
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <h5>Dados de sala fora</h3>

                    <br>

                    <div class="row">
                        <div class="col-9">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nome</th>
                                        <th scope="col">CPF</th>
                                        <th scope="col">Matricula</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Gestor auxiliar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <ul > -->
                                        <tr v-for="gestor in escola.gestor">
                                            <td>@{{ gestor.nome}}</th>
                                            <td>@{{ gestor.cpf}}</th>
                                            <td>@{{ gestor.matricula}}</th>
                                            <td>@{{ gestor.tipo_eleito}}</th>
                                        <td>@{{ gestor.cargo}}</th>
                                            
                                        </tr>
                                    <!-- </ul> -->

                                </tbody>
                            </table>
                        </div>
                        <div class="col-3">
                        <!-- FALTA ESSE TRECHO -->
                        </div>
                    </div>

                    <br>

                    <h5>Financeiro</h3>

                    <br>

                    <div class="row">
                        <div class="col-9">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Contratos e Repasses</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="#">Prestações de conta</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <br>

                    <h5>Contratos</h3>

                    <div class="row">
                        <div class="col-9">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Vigilância</th>
                                        <th scope="col">Serviços Gerais</th>
                                        <th scope="col">Aluguel</th>
                                        <th scope="col">Energia</th>
                                        <th scope="col">Merendeira</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr>
                                        <th>teste1</th>
                                        <td>teste2</td>
                                        <td>teste3</td>
                                        <td>teste4</td>
                                        <td>teste5</td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                        <div class="col-3">
                        <!-- FALTA ESSE TRECHO -->
                            <div class="card text-center">
                                <div class="card-header">
                                    Status financeiro
                                </div>
                                <div class="card-body">
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <h5>Repasses</h3>

                    <div class="row">
                        <div class="col-9">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Caixa Escolar</th>
                                        <th scope="col">PNAE</th>
                                        <th scope="col">PDDE</th>
                                        <th scope="col">Outros</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr>
                                        <th>teste1</th>
                                        <td>teste2</td>
                                        <td>teste3</td>
                                        <td>teste4</td>
                                        <td>teste5</td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                        <div class="col-3">
                        <!-- FALTA ESSE TRECHO -->
                        </div>
                    </div>
                </div>
                
            </div>

        </div>

        
        <!-- Importação do vue.js-->
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
        
        <script>

            var urlApi = "http://localhost:8000/api/dados";
            
            var app = new Vue ({ 
                el: '#app' , 
                data() { 
                    return {
                        escolas: [],
                        exibeDetalhamento:false
                    }
                },

                created: function(){
                    this.getUrlApi();
                },

                methods: {
                    //CONSULTA A URL DA API PARA EXIBIR OS DADOS RETORNADOS (TODAS AS ESCOLAS)
                    getUrlApi:function(){
                        this.$http.get(urlApi).then(function(response){
                            this.escolas = response.data.escolas;
                            console.log(response.data.escolas);
                        }).catch(function(erro){
                            console.log(erro);
                        })
                    },

                    //CONSULTA A API DE ACORDO COM O ID DA ESCOLA SELECIONADA
                    visualizar:function(escola){
                        this.$http.get("http://localhost:8000/api/dados/" + escola.codigo).then((response) => {
                            this.escola = response.data.escola;
                            this.exibeDetalhamento = true;
                            console.log(this.escola.terceirizados);
                        }).catch(function(erro){
                            console.log(erro);
                        })  
  
                    }

                }

            })
        </script>
    </body>
</html>