<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Escolas - Upload</title>
</head>
<body>
    <h3>Formulário</h3>
    <form action="{{url('/upload-imovel')}}" method="POST" enctype="multipart/form-data">
        
        <label>arquivo Imovel:</label> <br/>
        <input type="file" name="arquivo"/> <br/><br/>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <!-- <input type="submit" value="carregar"/> -->
    </form>
<hr>
    <form action="{{url('/upload-gestor')}}" method="POST" enctype="multipart/form-data">
        
        <label>arquivo Gestor:</label> <br/>
        <input type="file" name="arquivo"/> <br/><br/>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <!-- <input type="submit" value="carregar"/> -->
    </form>
<hr>
    <form action="{{url('/upload-terceirizado')}}" method="POST" enctype="multipart/form-data">

        <label>arquivo Terceirizado:</label> <br/>
        <input type="file" name="arquivo"/> <br/><br/>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <!-- <input type="submit" value="carregar"/> -->
    </form>
<hr>

     <form action="{{url('/upload-repasse')}}" method="POST" enctype="multipart/form-data">

        <label>arquivo Repasse:</label> <br/>
        <input type="file" name="arquivo"/> <br/><br/>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <!-- <input type="submit" value="carregar"/> -->
    </form>
</body>
</html>